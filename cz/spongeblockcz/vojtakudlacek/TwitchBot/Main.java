package cz.spongeblockcz.vojtakudlacek.TwitchBot;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

public class Main
{
	public static void startTimeLoop(IRCConnector TwitchBot) throws IOException, InterruptedException
	{
		long startTimeInSec = System.nanoTime()/1000/1000/1000;

		while(true)
		{
			long timeInSecs = (System.nanoTime()/1000/1000/1000);

			if((startTimeInSec - timeInSecs) % (5*60) == 0)
			{
				TwitchBot.sendMessage("�as b�hu: "+ ((timeInSecs - startTimeInSec)/60)  + " minut");
				Thread.sleep(1000);
			}else
			{
				Thread.sleep(100);
			}

		}
	}
}
class CommandHandler implements Runnable
{

	private IRCConnector TwitchBot;
	public CommandHandler(IRCConnector TwitchBot)
	{
		this.TwitchBot = TwitchBot;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void run()
	{
		long startTimeInSec = System.nanoTime()/1000/1000/1000;
		HashMap<String, String> commandStringList = new HashMap<>();
		try {
            BufferedReader br = new BufferedReader(new FileReader(new File("commands.json")));
            JSONObject commandsJson = new JSONObject(br.readLine());
            br.close();
            for (Object obj : commandsJson.toMap().values()) {
				commandStringList.putAll((HashMap<String, String>)obj);
			}
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



		while (true)
		{

			try {
				String chatCommand = TwitchBot.readLine();
				long timeInSecs = (System.nanoTime()/1000/1000/1000);
				String chatMessage = (chatCommand.split(":").length == 3) ? chatCommand.split(":")[2] : "";
				System.out.println(chatCommand);

				for (String strings : commandStringList.keySet()) {
				    if (chatMessage.contains(strings))
	                {
	                    TwitchBot.sendMessage(commandStringList.get(strings));
	                }
                }

				if (chatMessage.contains("!uptime"))
                {
                    TwitchBot.sendMessage("�as b�hu: "+ ((timeInSecs - startTimeInSec)/60 + 1) + " minut");
                }

				if(chatCommand.split(":")[0].contains("PING"))
				{
					TwitchBot.sendCommand("PONG :tmi.twitch.tv");
				}
				Thread.sleep(100);

			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
