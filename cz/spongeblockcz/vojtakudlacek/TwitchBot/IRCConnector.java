package cz.spongeblockcz.vojtakudlacek.TwitchBot;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class IRCConnector extends Socket{
	
	private String channel;
	public IRCConnector(String IPAddress, int port, String username, String pass) throws UnknownHostException, IOException
	{
		super(IPAddress, port);
		this.sendCommand("PASS " + pass);
		this.sendCommand("NICK " + username);
		//this.sendCommand("USER " + username + " 0 * " + username);
	}
	
	public IRCConnector connectToChannel(String channel) throws IOException
	{
		this.channel = channel;
		this.sendCommand("JOIN #" + channel);
		return this;
	}
	
	/**
	 * This function send irc command to the server
	 * !!USE AFTER connectToChannel!!
	 * @param command - IRC command
	 * @throws IOException
	 */
	public void sendCommand(String command) throws IOException
	{	
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(this.getOutputStream(),"UTF-8"));
		bw.write(command+"\n");
		bw.flush();
		
	}
	/**
	 * This will readLine from the chat (hopefully)
	 * @return Readed Line
	 * @throws IOException
	 */
	public String readLine() throws IOException
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(this.getInputStream(),"UTF-8"));
		return br.readLine();
	}

	public void sendMessage(String message) throws IOException {
		this.sendCommand("PRIVMSG #" + this.channel + " :" + message);
	}
	
}
