package cz.spongeblockcz.vojtakudlacek.TwitchBot;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import org.json.JSONObject;

public class CommandCreator {

	public static void main(String[] args)
    {
        Scanner scn =  new Scanner(System.in);
        String command = "";
        HashMap<String, String> commands = new HashMap<>();
        while(!command.equals("q"))
        {
            command = scn.nextLine();
            String body = scn.nextLine();
            if(!command.equals("q")) {
                commands.put(command, body);
            }

       }
        String json;
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("commands.json")));
            json = br.readLine();
            if(json == null)
            	throw new IOException();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
           json= "{}";
        }
        JSONObject jsonObject;
        if(json != "" || json != "{}" || json != null){
        	System.out.println(json);
        	jsonObject = new JSONObject(json);
	        for (Object strings : jsonObject.toMap().values()) {
	            commands.putAll((HashMap<String,String>) strings);
	        }
        }else
        {
        	jsonObject = new JSONObject();
        }
        System.out.println(commands);
        jsonObject.put("Commands", commands);
        System.out.println(jsonObject.toString());
        try {
            FileWriter fw = new FileWriter(new File("commands.json"));
            fw.append(jsonObject.toString());
            fw.flush();
            fw.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
